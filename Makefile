.DEFAULT_GOAL := start_dev
.PHONY: docker_generate docker_generate_command docker_requirements requirements diff

VENV ?= .venv

#
# Keys and certificates
#
generate_nginx_server_CA:
	cd nginx/keys_certificates/server && ./key_gen.sh

generate_nginx_client_CA:
	cd nginx/keys_certificates/client && ./root_key_gen.sh

generate_nginx_CA: generate_nginx_client_CA generate_nginx_server_CA

clear_CA:
	find . -name "*.pem" -type f -delete
	find . -name "*.srl" -type f -delete


#
# Build
#
build:
	docker compose -f docker-compose-build.yml build postgres web
	docker compose -f docker-compose-build.yml run --rm --no-deps web rm -rf /app/static
	docker compose -f docker-compose-build.yml run --rm --no-deps web python manage.py collectstatic
	docker compose -f docker-compose-build.yml build nginx

stop:
	docker compose -f docker-compose-build.yml down

start_dev:
	docker compose -f docker-compose-build.yml up -d


#
# Translations
#
makemessages:
	docker compose -f docker-compose-build.yml run --rm --no-deps web python manage.py makemessages --locale=ru

compilemessages:
	docker compose -f docker-compose-build.yml run --rm --no-deps web python manage.py compilemessages


### Dev stuff

update_migrations:
	docker compose -f docker-compose-build.yml run --rm --no-deps web python manage.py makemigrations
	docker compose -f docker-compose-build.yml run --rm --no-deps web python manage.py migrate

#
# Requirements
#
generate_requirements : docker_generate_requirements diff

docker_generate_requirements :
	docker run --rm -ti -v .:/project python:3.11.0 bash -c "cd /project/ && make VENV=/tmp/.venv docker_requirements"

docker_requirements : install_virtualenv requirements

install_virtualenv :
	pip install virtualenv==20.24.5

requirements :
	rm -rf ${VENV}
	virtualenv -p python3 ${VENV}
	${VENV}/bin/pip install --upgrade pip
	${VENV}/bin/pip install -r web/base-requirements.txt
	${VENV}/bin/pip freeze --all > web/requirements.txt

diff :
	echo "\nChanges in requirements.txt file\n"
	git diff web/requirements.txt

