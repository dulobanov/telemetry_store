from django.urls import path

from core.representations.bar_view import MeasurementBarView

urlpatterns = [
    path("measurements-bar-view/", MeasurementBarView.as_view()),
]
