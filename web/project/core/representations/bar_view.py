from random import randint

from django.db.models import Max, Min
from django.db.models.functions import Trunc
from django.views.generic import TemplateView
from core.models import Measurement

from core.api.measurement.view import MeasurementsFilterSet


class MeasurementBarView(TemplateView):
    template_name = 'bar_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        filtered_queryset = MeasurementsFilterSet(
            data=self.request.GET,
            queryset=Measurement.objects.select_related("data_source"),
            request=self.request,
        ).qs \
            .annotate(time=Trunc("datetime", "hour")) \
            .values("data_source", "data_source__name", "measurement", "time") \
            .annotate(min_value=Min("value"), max_value=Max("value")) \
            .order_by("time")  # Putting ordering before annotations leads to aggregation disabling

        # collecting all dates
        # changing data structure to next one
        # {
        #     "source_name": {
        #         "date_time": [<min_value>, <max_value>],
        #         ...
        #     }
        # }
        dataset_data_source = {}
        last_label = None
        x_labels = []
        for item in filtered_queryset:
            item_time = item["time"]
            if item_time != last_label:
                last_label = item_time
                x_labels.append(item_time)

            per_source_data = dataset_data_source.setdefault(
                "{}__{}".format(item["data_source__name"], item["measurement"]),
                {},
            )
            per_source_data[item["time"]] = [item["min_value"], item["max_value"]]

        # Preparing datasets for Chart.js
        datasets = []
        for source, source_data in dataset_data_source.items():
            source_data = {
                "label": source,
                "backgroundColor": "rgba({}, {}, {}, {})".format(
                    randint(50, 210),
                    randint(50, 210),
                    randint(50, 210),
                    randint(50, 210),
                ),
                "data": [source_data.get(x_label, []) for x_label in x_labels],
            }
            datasets.append(source_data)

        context["data"] = {
            "labels": [x_label.isoformat() for x_label in x_labels],
            "datasets": datasets,
        }

        return context
