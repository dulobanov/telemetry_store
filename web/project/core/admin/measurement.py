from django.contrib import admin

from core.models import Measurement


@admin.register(Measurement)
class MeasurementAdmin(admin.ModelAdmin):
    list_display = ("data_source", "created_on", "measurement", "datetime", "value")
