from django.urls import path

from core.hash_api.measurement.view import MeasurementsHashView

urlpatterns = [
    path("measurements/", MeasurementsHashView.as_view()),
]
