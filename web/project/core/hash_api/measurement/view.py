from core.api.measurement.view import MeasurementsView
from core.mixins.view_hash_auth_mixin import ViewHashAuthMixin


class MeasurementsHashView(ViewHashAuthMixin, MeasurementsView):
    pass
