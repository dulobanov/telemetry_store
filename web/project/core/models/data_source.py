from uuid import uuid4
from django.db import models


class DataSource(models.Model):
    fingerprint = models.CharField(max_length=128, primary_key=True, default=uuid4)
    hash_salt = models.CharField(max_length=128, unique=True, default=uuid4)
    name = models.CharField(max_length=128)
    location = models.ForeignKey("core.Location", on_delete=models.CASCADE)

    class Meta:
        unique_together = ["name", "location"]

    def __str__(self):
        return self.name
