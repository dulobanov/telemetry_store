from django.db import models


class Measurement(models.Model):
    data_source = models.ForeignKey("core.DataSource", on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_created=True)
    datetime = models.DateTimeField()
    measurement = models.CharField(max_length=128)
    value = models.FloatField()

    class Meta:
        unique_together = ["data_source", "datetime", "measurement"]
