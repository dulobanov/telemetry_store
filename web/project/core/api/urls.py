from django.urls import path

from core.api.measurement.view import MeasurementsView

urlpatterns = [
    path("measurements/", MeasurementsView.as_view()),
]
