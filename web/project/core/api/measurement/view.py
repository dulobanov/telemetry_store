from django.http import HttpResponseBadRequest
from django_filters import FilterSet, CharFilter, DateTimeFromToRangeFilter
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response

from core.api.measurement.serializer import MeasurementRetrieveSerializer, MeasurementCreateSerializer
from core.mixins.view_mixins import DynamicSerializerMixin
from core.models import Measurement


class MeasurementsFilterSet(FilterSet):
    datetime = DateTimeFromToRangeFilter()
    source = CharFilter(field_name="data_source__name")
    location = CharFilter(field_name="data_source__location")


class MeasurementsView(DynamicSerializerMixin, ListAPIView, CreateAPIView):
    queryset = Measurement.objects.select_related("data_source").order_by("datetime").all()
    create_serializer_class = MeasurementCreateSerializer
    serializer_class = MeasurementRetrieveSerializer
    filterset_class = MeasurementsFilterSet

    def create(self, request, *args, **kwargs):
        if isinstance(request.data, list):
            serializer = self.get_serializer(data=request.data, many=True)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return HttpResponseBadRequest("List of items expected.")
