from django.core.exceptions import ValidationError
from django.utils import timezone
from rest_framework.serializers import ModelSerializer, DateTimeField, CharField, HiddenField

from core.models import Measurement, DataSource


class DataSourceFingerprintFromHeader:
    requires_context = True

    def __call__(self, serializer_field):
        fingerprint = serializer_field.context['request'].META.get("HTTP_X_SSL_CLIENT_FINGERPRINT")
        if fingerprint:
            try:
                return DataSource.objects.get(fingerprint=fingerprint)
            except DataSource.DoesNotExist:
                raise ValidationError("DataSource with '{}' fingerprint does not exist.".format(fingerprint))
        else:
            raise ValidationError("DataSource fingerprint is not defined.")

    def __repr__(self):
        return '%s()' % self.__class__.__name__


class MeasurementRetrieveSerializer(ModelSerializer):
    source_name = CharField(source="data_source.name")
    source_location = CharField(source="data_source.location")

    class Meta:
        model = Measurement
        exclude = ["data_source"]


class MeasurementCreateSerializer(ModelSerializer):
    data_source = HiddenField(default=DataSourceFingerprintFromHeader())
    created_on = HiddenField(default=timezone.now)
    datetime = DateTimeField(required=True)
    source_name = CharField(source="data_source.name", read_only=True)
    source_location = CharField(source="data_source.location", read_only=True)

    class Meta:
        model = Measurement
        fields = "__all__"
