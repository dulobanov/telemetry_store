from rest_framework.generics import GenericAPIView


class DynamicSerializerMixin(GenericAPIView):
    retrieve_serializer_class = None
    create_serializer_class = None
    destroy_serializer_class = None
    update_serializer_class = None

    def get_serializer_class(self):
        method = self.request.method

        if method == "GET" and self.retrieve_serializer_class is not None:
            return self.retrieve_serializer_class
        elif method == "POST" and self.create_serializer_class is not None:
            return self.create_serializer_class
        elif method in ["PATCH", "PUT"] and self.update_serializer_class is not None:
            return self.update_serializer_class
        elif method == "DELETE" and self.destroy_serializer_class is not None:
            return self.destroy_serializer_class
        else:
            return super().get_serializer_class()
