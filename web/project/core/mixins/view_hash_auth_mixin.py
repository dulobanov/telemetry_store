from hashlib import sha256
from rest_framework import permissions

from core.models import DataSource


class HashPermission(permissions.BasePermission):
    HEADER_STATION_FINGERPRINT = "Station"
    HEADER_CONTENT_HASH = "Signature"
    HEADER_CONTENT_RAW_DATA = "X-Forwarded-Requestbody"

    def has_permission(self, request, view):
        result = False

        fingerprint = request.headers.get(self.HEADER_STATION_FINGERPRINT)
        content_hash = request.headers.get(self.HEADER_CONTENT_HASH)
        post_data = request.headers.get(self.HEADER_CONTENT_RAW_DATA)

        if all((fingerprint, content_hash, post_data)):
            try:
                data_source = DataSource.objects.get(fingerprint=fingerprint)
                expected_hash = sha256((post_data + data_source.hash_salt).encode()).digest().hex()

                if expected_hash == content_hash:
                    request.META["HTTP_X_SSL_CLIENT_FINGERPRINT"] = fingerprint
                    result = True
            except DataSource.DoesNotExist:
                pass

        return result


class ViewHashAuthMixin:
    permission_classes = (HashPermission,)
