#!/usr/bin/env bash

ROOT_KEY_LENGTH=8192
INTERMEDIATE_KEY_LENGTH=8192

CERTIFICATE_TTL=3652

rm -rf *.pem
rm -rf *.srl

echo "Generating Root CA"
openssl req -new -config CA.cnf -sha256 -newkey rsa:${ROOT_KEY_LENGTH} -nodes -keyout CA.key.pem -out CA.csr.pem
openssl x509 -req -days ${CERTIFICATE_TTL} -in CA.csr.pem -extfile CA.cnf -extensions ext -signkey CA.key.pem -CAcreateserial -out CA.crt.pem

echo "Generating Nginx Client root key & certificate"
openssl req -new -config client_intermediate.cnf -sha256 -newkey rsa:${INTERMEDIATE_KEY_LENGTH} -nodes -keyout client_intermediate.key.pem -out client_intermediate.csr.pem
openssl x509 -req -days ${CERTIFICATE_TTL} -in client_intermediate.csr.pem -extfile client_intermediate.cnf -extensions ext -CA CA.crt.pem -CAkey CA.key.pem -CAcreateserial -out client_intermediate.crt.pem
openssl verify -verbose -CAfile CA.crt.pem client_intermediate.crt.pem

echo "Removing CSR files"
rm -f *.csr.pem

echo "Add read permission to keys"
chmod +r client_intermediate.key.pem