#!/usr/bin/env bash

if [ -z "$1" ]
then
  echo "Please clarify client name."
  exit 1
fi

if [ ! -d $1 ]
then
  echo "Generating Key & Certificate for '$1' client."
  ./client_key_gen.sh $1
fi

echo "Generating p12 package"
openssl pkcs12 -export -out $1/client-$1.p12 -inkey $1/client-$1.key.pem -in $1/client-$1.crt.pem -certfile CA.crt.pem
