#!/usr/bin/env bash

if [ -z "$1" ]
then
  echo "Please clarify client name."
  exit 1
fi

if [ -d $1 ]
then
  echo "Certificate for '$1' client is already exist."
  exit 2
fi

Client_KEY_LENGTH=4096

CERTIFICATE_TTL=3652

echo "Creating client.cnf"
mkdir $1

cat >> $1/client-$1.cnf <<EOF
[req]
prompt = no
distinguished_name = dn
req_extensions = req_ext

[dn]
CN = $1-TelemetryStoreClient
O = TelemetryStore
OU = TelemetryStore

[req_ext]
basicConstraints = CA:false
subjectKeyIdentifier=hash

[ext]
# PKIX recommendation.
basicConstraints = CA:false
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
keyUsage = digitalSignature,keyEncipherment
extendedKeyUsage=clientAuth
EOF

echo "Generating Client key & certificate"
openssl req -new -config $1/client-$1.cnf -sha256 -newkey rsa:${Client_KEY_LENGTH} -nodes -keyout $1/client-$1.key.pem -out $1/client-$1.csr.pem
openssl x509 -req -days ${CERTIFICATE_TTL} -in $1/client-$1.csr.pem -extfile $1/client-$1.cnf -extensions ext -CA client_intermediate.crt.pem -CAkey client_intermediate.key.pem -CAcreateserial -out $1/client-$1-single.crt.pem

# Composing client full chain certificate
cat $1/client-$1-single.crt.pem client_intermediate.crt.pem > $1/client-$1.crt.pem
rm -f $1/client-$1-single.crt.pem

echo "Validating certificate"
openssl verify -verbose -CAfile CA.crt.pem $1/client-$1.crt.pem

echo "Calculating fingerprint"
openssl x509 -in $1/client-$1.crt.pem -outform der | sha1sum > $1/client-$1-fingerprint.txt

echo "Removing CSR & CNF temporary files"
rm -f $1/*.csr.pem
rm -f $1/*.cnf

echo "Add read permission to keys"
chmod +r $1/client-$1.key.pem
