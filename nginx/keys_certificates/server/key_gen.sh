#!/usr/bin/env bash

ROOT_KEY_LENGTH=8192
SERVER_KEY_LENGTH=4096

CERTIFICATE_TTL=3652

rm -rf *.pem
rm -rf *.srl

echo "Generating Root CA"
openssl req -new -config CA.cnf -sha256 -newkey rsa:${ROOT_KEY_LENGTH} -nodes -keyout CA.key.pem -out CA.csr.pem
openssl x509 -req -days ${CERTIFICATE_TTL} -in CA.csr.pem -extfile CA.cnf -extensions ext -signkey CA.key.pem -CAcreateserial -out CA.crt.pem

echo "Generating Nginx key certificate"
openssl req -new -config nginx.cnf -sha256 -newkey rsa:${SERVER_KEY_LENGTH} -nodes -keyout nginx.key.pem -out nginx.csr.pem
openssl x509 -req -days ${CERTIFICATE_TTL} -in nginx.csr.pem -extfile nginx.cnf -extensions ext -CA CA.crt.pem -CAkey CA.key.pem -CAcreateserial -out nginx.crt.pem
openssl verify -verbose -CAfile CA.crt.pem nginx.crt.pem

echo "Removing CSR files"
rm -f *.csr.pem

echo "Add read permission to keys"
chmod +r nginx.key.pem